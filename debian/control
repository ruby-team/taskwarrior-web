Source: taskwarrior-web
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Ben Armstrong <synrg@sanctuary.nslug.ns.ca>
Build-Depends: debhelper (>= 9~),
               gem2deb,
               rake,
               ruby-rack-test,
               ruby-rspec
Standards-Version: 3.9.7
Vcs-Git: https://anonscm.debian.org/git/pkg-ruby-extras/taskwarrior-web.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-ruby-extras/taskwarrior-web.git
Homepage: http://github.com/theunraveler/taskwarrior-web
XS-Ruby-Versions: ruby1.9.1

Package: taskwarrior-web
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: libjs-jquery,
         libjs-jquery-hotkeys,
         libjs-twitter-bootstrap,
         ruby-activesupport-3.2,
         ruby-json,
         ruby-parseconfig,
         ruby-rack-flash3,
         ruby-rinku,
         ruby-sinatra,
         ruby-sinatra-simple-navigation,
         ruby-vegas,
         ruby-versionomy,
         ruby1.9.1,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Web frontend for the Taskwarrior todo application
 A lightweight, Sinatra-based web interface for the wonderful Taskwarrior todo
 application.
 .
 The current featureset includes:
 .
 * Viewing tasks sorted and grouped in various ways.
 * Creating a new task with a due date, project, and tags.
 * Editing and deleting tasks.
 * Uses your task config to determine date formatting and when an upcoming task
   should be marked as "due".
 * Optional HTTP Basic authentication.
